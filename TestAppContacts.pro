TEMPLATE = app
QT      += quick network

# Exclude the following not needed framework modules
DEFINES += EXCLUDE_HARDWAREKEYS
DEFINES += EXCLUDE_MEDIAPLATFORMONITOR
DEFINES += EXCLUDE_MEDIAPLAYER
DEFINES += EXCLUDE_NAVIGATION
DEFINES += EXCLUDE_PHONE
DEFINES += EXCLUDE_AUDIOINPUT
DEFINES += EXCLUDE_XMLPARSER
DEFINES += EXCLUDE_SPEECH
DEFINES += EXCLUDE_SPATILEMAP

SOURCES += main.cpp \
    CloudSettings.cpp \
    controllers/AppController.cpp \
    controllers/MaximizedViewController.cpp

qmlsources.source   = qml
qmlsources.target   = .
DEPLOYMENTFOLDERS   += qmlsources

include (../SpaApplicationFramework/src/SpaApplicationFramework.pri)

OTHER_FILES += \
    ManifestFile.ini \
    qml/main.qml \
    qml/views/*

hack_for_lupdate {
    SOURCES += $$OTHER_FILES
}

applicationDeployment()

HEADERS += \
    AppConfig.h \
    CloudSettings.h \
    controllers/AppController.h \
    controllers/MaximizedViewController.h

QML_IMPORT_PATH += $$PWD
