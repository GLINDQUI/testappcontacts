#ifndef CLOUDSETTINGS_H
#define CLOUDSETTINGS_H

#include "SpaApplicationViewer.h"
#include <QObject>

class CloudSettings : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool processingData READ processingData WRITE setProcessingData NOTIFY processingDataChanged)

public:
    explicit CloudSettings(QObject *parent = 0);
    explicit CloudSettings(SpaApplicationViewer *viewer);

    bool processingData() const;

signals:
    void gotCloudSettings();
    void processingDataChanged(bool processingData);

public slots:
    void onFeaturesLoaded();
    void setProcessingData(bool processingData);

private:
    SpaApplicationViewer *_viewer;
    bool _processingData;
};

#endif // CLOUDSETTINGS_H
