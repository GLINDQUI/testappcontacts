import QtQuick 2.0
import SpaComponents 1.0
import AppControllerEnums 1.0
import "views"

SpaApplicationArea {
    id: appContainer

    MaximizedView {
        id: maximizedView
    }

    Component.onCompleted: {
        //: Light 24 450 1
        //: App title
        //% "Test app for phone contacts"
        Viewer.mainText = qsTrId("668_APP_TITLE")
    }
}
