#ifndef MAXIMIZEDVIEWCONTROLLER_H
#define MAXIMIZEDVIEWCONTROLLER_H

#include "AppController.h"
#include <QObject>

class MaximizedViewController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool updating READ updating NOTIFY updatingChanged)

public:
    explicit MaximizedViewController(QObject *parent = 0);
    explicit MaximizedViewController(AppController *appController, QObject *parent = 0);

    Q_INVOKABLE void updateContactsButtonClicked();
    Q_INVOKABLE void updateCountsButtonClicked();
    Q_INVOKABLE bool updating() { return m_updating; }

signals:
    void updatingChanged(bool m_updating);

private:
    AppController *m_appController;
    bool m_updating;

    void setUpdating(bool value);
};

#endif // MAXIMIZEDVIEWCONTROLLER_H
