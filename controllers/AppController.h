#ifndef APPCONTROLLER_H
#define APPCONTROLLER_H

#include "SpaApplicationViewer.h"
#include <QObject>

class AppController : public QObject
{
    Q_OBJECT

public:
    AppController(QObject *parent = 0);
    explicit AppController(SpaApplicationViewer *viewer);

    void updateContacts();

public slots:
    void updateCounts();

private:
    SpaApplicationViewer *_viewer;
};

#endif // APPCONTROLLER_H
