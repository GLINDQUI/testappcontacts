#include "MaximizedViewController.h"
#include "Log.h"

MaximizedViewController::MaximizedViewController(QObject *parent) : QObject(parent) { }

MaximizedViewController::MaximizedViewController(AppController *appController, QObject *parent) : QObject(parent),
    m_appController(appController),
    m_updating(false) { }

void MaximizedViewController::updateContactsButtonClicked() {
    setUpdating(true);
    m_appController->updateContacts();
    setUpdating(false);
}

void MaximizedViewController::updateCountsButtonClicked() {
    m_appController->updateCounts();
}

void MaximizedViewController::setUpdating(bool value) {
    if (m_updating != value) {
        m_updating = value;
        emit updatingChanged(m_updating);
    }
}
