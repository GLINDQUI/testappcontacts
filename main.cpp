#include "SpaApplication.h"
#include "SpaApplicationViewer.h"
#include "SpaCloud.h"
#include "CloudSettings.h"
#include "SpaPlatform/SpaPlatform.h"
#include "controllers/AppController.h"
#include "controllers/MaximizedViewController.h"

#include <QQmlContext>
#include <QtQml>

int main(int argc, char *argv[])
{
    // Create the QGuiApplication and QQuickView
    SpaApplication app(argc, argv);
    SpaApplicationViewer *viewer = SpaApplicationViewer::instance();

    // Cloud settings
    CloudSettings *cloudSettings = new CloudSettings(viewer);

    // Appcontroller
    AppController *appController = new AppController(viewer);

    // View controllers
    MaximizedViewController *maximizedViewController = new MaximizedViewController(appController, viewer);

    // To be able to use enums in QML
    qmlRegisterType<AppController>("AppControllerEnums", 1, 0, "AppController");

    // To be able to reach objects from QML
    viewer->rootContext()->setContextProperty("cloudSettings", cloudSettings);
    viewer->rootContext()->setContextProperty("appController", appController);
    viewer->rootContext()->setContextProperty("maximizedViewController", maximizedViewController);

    // QML for viewer
    viewer->setSource(QStringLiteral("qml/main.qml"));
    viewer->show();

    // Main event loop
    return app.exec();
}
