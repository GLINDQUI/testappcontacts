#include "AppController.h"
#include "Log.h"
#include "FrameworkInterface.h"
#include "ContactsModel.h"

AppController::AppController(QObject *parent) : QObject(parent) { }

AppController::AppController(SpaApplicationViewer *viewer) : QObject(viewer),
    _viewer(viewer) {
    connect(_viewer->framework()->contacts(), SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)), this, SLOT(updateCounts()));
    connect(_viewer->framework()->contacts(), SIGNAL(modelReset()), this, SLOT(updateCounts()));
    connect(_viewer->framework()->contacts(), SIGNAL(rowsInserted(QModelIndex,int,int)), this, SLOT(updateCounts()));
    connect(_viewer->framework()->contacts(), SIGNAL(rowsRemoved(QModelIndex,int,int)), this, SLOT(updateCounts()));

    updateCounts();
}

void AppController::updateCounts()
{
    Log::info(QStringLiteral("AppController"), QStringLiteral("updateCounts"), QStringLiteral("updating contact counts in tile header"));

    JavascriptInterfaceContacts* contacts = _viewer->framework()->contacts();

    QStringList uniqueContacts;
    for (int i = 0; i < contacts->count(); i++) {
        QVariantMap c = contacts->getContact(i).toMap();
        QString value = QString("%1%2").arg(c.value("name").toString()).arg(c.value("number").toString());
        if (!uniqueContacts.contains(value)) {
            uniqueContacts.append(value);
        }
    }

    _viewer->setSubText(QStringLiteral("%1 unique contacts, %2 total").arg(uniqueContacts.count()).arg(contacts->count()));
}

void AppController::updateContacts()
{
    Log::info(QStringLiteral("AppController"), QStringLiteral("updateContacts"), QStringLiteral("requesting to update contacts"));

    _viewer->framework()->contacts()->update();
}
