import QtQuick 2.0
import SpaComponents 1.0
import SpaHmiEnums 1.0

SpaMaximizedView {
    id: root

    visible: Viewer.renderState === SpaApplicationViewer.RenderStateExpanded

    SpaHomescreenToolbarBase {
        id: toolbar

        anchors.top: maximizedView.top
        anchors.left: maximizedView.left
        anchors.bottom: maximizedView.bottom
        anchors.topMargin: HmiProvider.fetchProperty(HmiEnums.SPAHOMESCREENTOOLBAR_TOPMARGIN)
        anchors.leftMargin: HmiProvider.fetchProperty(HmiEnums.SPAHOMESCREENTOOLBAR_LEFTMARGIN)

        textButtonCount: 2
        type: "002"

        //: Light 24 200 1
        //% "Update contacts"
        buttonText1: qsTrId("668_UPDATE_CONTACTS")
        buttonIcon1: Theme.path + "icons/refresh.png"

        //: Light 24 200 1
        //% "Update counts"
        buttonText2: qsTrId("668_UPDATE_COUNTS")
        buttonIcon2: Theme.path + "icons/search.png"

        onButton1Clicked: maximizedViewController.updateContactsButtonClicked()
        onButton2Clicked: maximizedViewController.updateCountsButtonClicked()
    }

    SpaHomescreenList {
        id: homescreenList

        anchors.left: toolbar.right
        anchors.right: maximizedView.right
        anchors.top: maximizedView.top
        anchors.bottom: maximizedView.bottom
        anchors.leftMargin: HmiProvider.fetchProperty(HmiEnums.SPAHOMESCREENLIST_LEFTMARGIN)
        anchors.rightMargin: HmiProvider.fetchProperty(HmiEnums.SPAHOMESCREENLIST_RIGHTMARGIN)
        anchors.topMargin: HmiProvider.fetchProperty(HmiEnums.SPAHOMESCREENLIST_TOPMARGIN)

        //: Light 24 300 1
        //% "Contacts"
        title: qsTrId("668_CONTACTS")
        model: Contacts
        delegate: Component {
            SpaListItem {
                itemType: "00.01.02"
                height: 48
                rightTextLabelWidth: 300
                clickable: false
                leftText: model.name
                rightText: model.number
            }
        }
    }

    SpaActivityIndicatorLarge {
        id: spinner
        anchors.centerIn: homescreenList
        showPercent: false
        infinite: true
    }

    states: [
        State {
            name: "updating"
            when: maximizedViewController.updating

            PropertyChanges {
                target: toolbar
                enabled: false
            }

            PropertyChanges {
                target: homescreenList
                visible: false
            }

            PropertyChanges {
                target: spinner
                visible: true
            }
        }
    ]
}
