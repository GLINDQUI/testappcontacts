#include "CloudSettings.h"
#include "SpaCloud.h"
#include "Log.h"

CloudSettings::CloudSettings(QObject *parent) : QObject(parent)
{
}

CloudSettings::CloudSettings(SpaApplicationViewer *viewer) : QObject(viewer),
    _viewer(viewer)
{
    connect(viewer->cloud(), &SpaCloud::featuresLoaded, this, &CloudSettings::onFeaturesLoaded);
    setProcessingData(true);
}

bool CloudSettings::processingData() const
{
    return _processingData;
}

void CloudSettings::onFeaturesLoaded()
{
    Log::info(QStringLiteral("CloudSettings"), QStringLiteral("onFeaturesLoaded"), QStringLiteral("baseUrl = %1, featureUri = %2").arg(_viewer->cloud()->getBaseUrl()).arg(_viewer->cloud()->getFeatureUri(_viewer->cloud()->getFeatureName("scorfaproxy"))));
    gotCloudSettings();
    setProcessingData(false);
}

void CloudSettings::setProcessingData(bool processingData)
{
    if (_processingData == processingData) {
        return;
    }

    _processingData = processingData;
    emit processingDataChanged(processingData);
}
